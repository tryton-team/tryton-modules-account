tryton-modules-account (7.0.16-1) unstable; urgency=medium

  * Merging upstream version 7.0.16.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Jan 2025 11:29:03 +0100

tryton-modules-account (7.0.15-1) unstable; urgency=medium

  * Merging upstream version 7.0.15.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 02 Jan 2025 14:40:00 +0100

tryton-modules-account (7.0.12-3) unstable; urgency=medium

  * Use unittest.discover to run autopkgtests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 25 Oct 2024 12:55:14 +0200

tryton-modules-account (7.0.12-2) experimental; urgency=medium

  * Upload to experimental.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 21 Oct 2024 07:55:17 +0200

tryton-modules-account (7.0.12-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.7.0, no changes needed.
  * Setting the branch in the watch file to the fixed version 7.0.
  * Remove deprecated python3-pkg-resources from (Build)Depends (and wrap-
    and-sort -a) (Closes: #1083808).
  * Merging upstream version 7.0.12.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Oct 2024 12:10:59 +0200

tryton-modules-account (6.0.27-1) unstable; urgency=medium

  * Merging upstream version 6.0.27.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 18 Sep 2024 14:09:39 +0200

tryton-modules-account (6.0.26-1) unstable; urgency=medium

  * Merging upstream version 6.0.26.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Jul 2024 10:52:38 +0200

tryton-modules-account (6.0.24-1) unstable; urgency=medium

  * Switch to pgpmode=none in the watch file.
  * Merging upstream version 6.0.24.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 03 May 2024 12:17:15 +0200

tryton-modules-account (6.0.23-1) unstable; urgency=medium

  * Switch to pgpmode=auto in the watch file.
  * Update year of debian copyright.
  * Merging upstream version 6.0.23.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 18 Apr 2024 11:16:42 +0200

tryton-modules-account (6.0.21-1) unstable; urgency=medium

  * Merging upstream version 6.0.21.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 11 Jan 2024 12:08:31 +0100

tryton-modules-account (6.0.18-1) unstable; urgency=medium

  * Merging upstream version 6.0.18.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 16 Oct 2023 13:08:21 +0200

tryton-modules-account (6.0.17-1) unstable; urgency=medium

  * Merging upstream version 6.0.17.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 27 Jun 2023 14:52:13 +0200

tryton-modules-account (6.0.15-1) unstable; urgency=medium

  * Merging upstream version 6.0.15.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Feb 2023 14:36:25 +0100

tryton-modules-account (6.0.14-1) unstable; urgency=medium

  * Add a salsa-ci.yml
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Repository.
  * Update standards version to 4.6.1, no changes needed.
  * Unify the Tryton module layout.
  * Update the package URLS to https and the new mono repos location.
  * Bump the Standards-Version to 4.6.2, no changes needed.
  * Merging upstream version 6.0.14.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 08 Feb 2023 13:02:35 +0100

tryton-modules-account (6.0.13-1) unstable; urgency=medium

  * Merging upstream version 6.0.13.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 09 Sep 2022 10:24:03 +0200

tryton-modules-account (6.0.11-1) unstable; urgency=medium

  * Merging upstream version 6.0.11.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 08 Jun 2022 10:53:56 +0200

tryton-modules-account (6.0.9-1) unstable; urgency=medium

  * Merging upstream version 6.0.9.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 10 Mar 2022 10:26:51 +0100

tryton-modules-account (6.0.5-1) unstable; urgency=medium

  * Merging upstream version 6.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 18 Dec 2021 11:40:51 +0100

tryton-modules-account (6.0.3-2) unstable; urgency=medium

  * Use debhelper-compat (=13).
  * Depend on the tryton-server-api of the same major version.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 12 Nov 2021 21:52:36 +0100

tryton-modules-account (6.0.3-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.6.0, no changes needed.
  * Set the watch file version to 4.
  * Setting the branch in the watch file to the fixed version 6.0.
  * Merging upstream version 6.0.3.
  * Use same debhelper compat as for server and client.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 19 Oct 2021 12:11:55 +0200

tryton-modules-account (5.0.18-1) unstable; urgency=medium

  * Updating to standards version 4.5.1, no changes needed.
  * Merging upstream version 5.0.18.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 29 Jun 2021 11:55:58 +0200

tryton-modules-account (5.0.16-1) unstable; urgency=medium

  * Merging upstream version 5.0.16.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Dec 2020 10:04:06 +0100

tryton-modules-account (5.0.14-1) unstable; urgency=medium

  * Merging upstream version 5.0.14.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 26 Sep 2020 10:03:59 +0200

tryton-modules-account (5.0.13-1) unstable; urgency=medium

  * Update year of debian copyright.
  * Bump the Standards-Version to 4.5.0, no changes needed.
  * Add Rules-Requires-Root: no to d/control.
  * Merging upstream version 5.0.13.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 04 Jul 2020 12:49:43 +0200

tryton-modules-account (5.0.11-1) unstable; urgency=medium

  * Merging upstream version 5.0.11.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Apr 2020 13:31:31 +0200

tryton-modules-account (5.0.10-1) unstable; urgency=medium

  * Add the correct license for the distributed icons.
  * Merging upstream version 5.0.10.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 12 Mar 2020 17:57:29 +0100

tryton-modules-account (5.0.9-1) unstable; urgency=medium

  * Merging upstream version 5.0.9.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Dec 2019 09:18:43 +0100

tryton-modules-account (5.0.8-1) unstable; urgency=medium

  * Merging upstream version 5.0.8.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 23 Jul 2019 19:19:01 +0200

tryton-modules-account (5.0.5-2) unstable; urgency=medium

  * Bump the Standards-Version to 4.4.0, no changes needed.
  * Update year of debian copyright.
  * Setting the branch in the watch file to the fixed version 5.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 22 Jul 2019 12:16:31 +0200

tryton-modules-account (5.0.5-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 5.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 03 Apr 2019 13:54:12 +0200

tryton-modules-account (5.0.4-1) unstable; urgency=medium

  * Merging upstream version 5.0.4.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 20 Feb 2019 11:51:46 +0100

tryton-modules-account (5.0.3-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.3.0, no changes needed.
  * Merging upstream version 5.0.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 06 Jan 2019 12:47:16 +0100

tryton-modules-account (5.0.2-2) unstable; urgency=medium

  * Add a generic autopkgtest to run the module testsuite.
  * Update the rules file with hints about and where to run tests.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 01 Jan 2019 20:34:46 +0100

tryton-modules-account (5.0.2-1) unstable; urgency=medium

  * Cleanup white space.
  * Merging upstream version 5.0.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 03 Dec 2018 14:03:10 +0100

tryton-modules-account (5.0.1-1) unstable; urgency=medium

  * Merging upstream version 5.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 25 Nov 2018 00:43:59 +0100

tryton-modules-account (5.0.0-1) unstable; urgency=medium

  * Merging upstream version 5.0.0.
  * Updating to Standards-Version: 4.2.1, no changes needed.
  * Update signing-key.asc with the minimized actual upstream maintainer
    key.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 20 Nov 2018 17:07:41 +0100

tryton-modules-account (4.6.3-1) unstable; urgency=medium

  * Merging upstream version 4.6.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 04 Apr 2018 18:51:58 +0200

tryton-modules-account (4.6.2-1) unstable; urgency=medium

  * Merging upstream version 4.6.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 30 Mar 2018 00:54:38 +0200

tryton-modules-account (4.6.0-2) unstable; urgency=medium

  * Update year of debian copyright.
  * Updating to standards version 4.1.3, no changes needed.
  * control: update Vcs-Browser and Vcs-Git
  * Set the Maintainer address to team+tryton-team@tracker.debian.org.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 29 Mar 2018 21:14:23 +0200

tryton-modules-account (4.6.0-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.1, no changes needed.
  * Merging upstream version 4.6.0.
  * Use https in the watch file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 07 Nov 2017 10:19:42 +0100

tryton-modules-account (4.4.2-1) unstable; urgency=medium

  * Bump the Standards-Version to 4.1.0, no changes needed.
  * Merging upstream version 4.4.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 05 Oct 2017 11:33:17 +0200

tryton-modules-account (4.4.1-3) unstable; urgency=medium

  * Switch to Python3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 18 Aug 2017 12:35:10 +0200

tryton-modules-account (4.4.1-2) unstable; urgency=medium

  * Use the preferred https URL format in the copyright file.
  * Bump the Standards-Version to 4.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 16:02:28 +0200

tryton-modules-account (4.4.1-1) unstable; urgency=medium

  * Merging upstream version 4.4.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 17 Aug 2017 13:13:03 +0200

tryton-modules-account (4.4.0-2) unstable; urgency=medium

  * Change the maintainer address to tryton-debian@lists.alioth.debian.org
    (Closes: #865109).

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Jul 2017 12:35:54 +0200

tryton-modules-account (4.4.0-1) unstable; urgency=medium

  * Add the actual upstream maintainer key to signing-key.asc.
  * Merging upstream version 4.4.0.
  * Updating debian/copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 10 Jun 2017 23:29:22 +0200

tryton-modules-account (4.2.0-1) unstable; urgency=medium

  * Merging upstream version 4.2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 05 Dec 2016 15:31:01 +0100

tryton-modules-account (4.0.5-1) unstable; urgency=medium

  * Merging upstream version 4.0.5.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Nov 2016 09:27:43 +0100

tryton-modules-account (4.0.4-1) unstable; urgency=medium

  * Merging upstream version 4.0.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 03 Oct 2016 12:35:12 +0200

tryton-modules-account (4.0.3-1) unstable; urgency=medium

  * Merging upstream version 4.0.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 03 Sep 2016 20:46:11 +0200

tryton-modules-account (4.0.2-1) unstable; urgency=medium

  * Updating to Standards-Version: 3.9.8, no changes needed.
  * Merging upstream version 4.0.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 05 Aug 2016 11:37:10 +0200

tryton-modules-account (4.0.1-1) unstable; urgency=medium

  * Updating signing-key.asc with the actual upstream maintainer keys.
  * Merging upstream version 4.0.0.
  * Merging upstream version 4.0.1.
  * Updating the copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 30 May 2016 19:25:38 +0200

tryton-modules-account (3.8.1-2) unstable; urgency=medium

  * Updating to standards version 3.9.7, no changes needed.
  * Updating VCS-Browser to https and canonical cgit URL.
  * Updating VCS-Git to https and canonical cgit URL.
  * Removing the braces from the dh call.
  * Removing the version constraint from python.
  * Disable tests globally for all Python versions.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 08 Mar 2016 13:08:19 +0100

tryton-modules-account (3.8.1-1) unstable; urgency=medium

  * Merging upstream version 3.8.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 23 Dec 2015 14:52:19 +0100

tryton-modules-account (3.8.0-1) unstable; urgency=medium

  * Merging upstream version 3.8.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 12 Nov 2015 19:12:57 +0100

tryton-modules-account (3.6.2-1) unstable; urgency=medium

  * Enabling tests on sqlite memory database.
  * Revert "Enabling tests on sqlite memory database."
  * Improving description why we can not run the module test suites.
  * Merging upstream version 3.6.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 20 Sep 2015 21:14:45 +0200

tryton-modules-account (3.6.1-1) unstable; urgency=medium

  * Updating year of debian copyright.
  * Adapting section naming in gbp.conf to current git-buildpackage.
  * Merging upstream version 3.6.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 15 Jul 2015 14:02:21 +0200

tryton-modules-account (3.6.0-1) unstable; urgency=medium

  * Wrapping and sorting control files (wrap-and-sort -bts).
  * Merging upstream version 3.6.0.
  * Updating Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 26 Apr 2015 23:48:55 +0200

tryton-modules-account (3.4.2-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Improving boilerplate in d/control (Closes: #771722).
  * Merging upstream version 3.4.2.
  * Updating copyright file.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 03 Mar 2015 12:29:25 +0100

tryton-modules-account (3.4.1-1) unstable; urgency=medium

  * Merging upstream version 3.4.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 04 Dec 2014 19:25:42 +0100

tryton-modules-account (3.4.0-1) unstable; urgency=medium

  * Adding actual upstream signing key.
  * Updating to Standards-Version: 3.9.6, no changes needed.
  * Merging upstream version 3.4.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 21 Oct 2014 20:23:38 +0200

tryton-modules-account (3.2.1-2) unstable; urgency=medium

  * Removing TODO from docs, it is no more in the package.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 31 Aug 2014 16:59:52 +0200

tryton-modules-account (3.2.1-1) unstable; urgency=medium

  * Updating signing key while using now plain .asc files instead of .pgp
    binaries.
  * Merging upstream version 3.2.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 26 Aug 2014 14:48:55 +0200

tryton-modules-account (3.2.0-1) unstable; urgency=medium

  * Merging upstream version 3.2.0.
  * Bumping minimal required Python version to 2.7.
  * Updating gbp.conf for usage of upstream tarball compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 24 Apr 2014 15:25:23 +0200

tryton-modules-account (3.0.3-1) unstable; urgency=medium

  * Merging upstream version 3.0.3.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 16 Apr 2014 15:10:48 +0200

tryton-modules-account (3.0.2-1) unstable; urgency=medium

  * Removing  LC_ALL=C.UTF-8 as build environment.
  * Merging upstream version 3.0.2.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 26 Mar 2014 18:48:50 +0100

tryton-modules-account (3.0.1-2) unstable; urgency=medium

  * Updating year in debian copyright.
  * Removing debian/source/options, we are building with dpkg defaults.
  * Removing PYBUILD_DESTDIR_python2 from rules, it is no more needed.
  * Adding pgp verification for uscan.
  * Adding gbp.conf for usage with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Mar 2014 17:13:18 +0100

tryton-modules-account (3.0.1-1) unstable; urgency=low

  * Merging upstream version 3.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 11 Dec 2013 12:59:43 +0100

tryton-modules-account (3.0.0-2) unstable; urgency=low

  * Pointing VCS fields to new location on alioth.debian.org.
  * Using dpkg defaults for xz compression.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 02 Dec 2013 21:14:59 +0100

tryton-modules-account (3.0.0-1) unstable; urgency=low

  * Removing inadvertently commited .pc directory.
  * Merging upstream version 3.0.0.
  * Updating to standards version 3.9.5, no changes needed.
  * Changing to buildsystem pybuild.
  * Adding python-sql to Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 25 Nov 2013 17:52:57 +0100

tryton-modules-account (2.8.2-1) unstable; urgency=low

  * Merging upstream version 2.8.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 13 Oct 2013 20:21:21 +0200

tryton-modules-account (2.8.1-2) unstable; urgency=low

  * Removing pydist-overrides, it is no more needed.
  * Adapting the rules file to work also with git-buildpackage.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 06 Aug 2013 13:32:02 +0200

tryton-modules-account (2.8.1-1) unstable; urgency=low

  * Merging upstream version 2.8.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 10 Jun 2013 13:52:18 +0200

tryton-modules-account (2.8.0-2) unstable; urgency=low

  * Adding doc/ to docs file.
  * Simplifying package layout by renaming <pkg_name>.docs to docs.
  * Removing needless empty line in rules.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 31 May 2013 17:25:54 +0200

tryton-modules-account (2.8.0-1) experimental; urgency=low

  * Merging upstream version 2.8.0.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 02 May 2013 15:19:21 +0200

tryton-modules-account (2.6.3-3) experimental; urgency=low

  * Removing Daniel from Uploaders. Thanks for your work! (Closes: #704361).
  * Improving update of major version in Depends.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 27 Apr 2013 15:03:54 +0200

tryton-modules-account (2.6.3-2) experimental; urgency=low

  * Updating Vcs-Git to correct address.
  * Adding watch file. Thanks to Bart Martens <bartm@debian.org>.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 23 Mar 2013 13:59:59 +0100

tryton-modules-account (2.6.3-1) experimental; urgency=low

  * Removing obsolete Dm-Upload-Allowed
  * Updating to Standards-Version: 3.9.4, no changes needed.
  * Merging upstream version 2.6.2.
  * Merging upstream version 2.6.3.
  * Updating copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 16 Feb 2013 21:33:41 +0100

tryton-modules-account (2.6.1-1) experimental; urgency=low

  * Merging upstream version 2.6.0.
  * Bumping versioned tryton depends to 2.6.
  * Merging upstream version 2.6.1.
  * Adding xz compression in source/options.

 -- Mathias Behrle <mathiasb@m9s.biz>  Wed, 24 Oct 2012 13:59:38 +0200

tryton-modules-account (2.4.2-2) experimental; urgency=low

  [ Daniel Baumann ]
  * Updating maintainers field.
  * Updating vcs fields.
  * Correcting copyright file to match format version 1.0.
  * Switching to xz compression.
  * Updating to debhelper version 9.

  [ Mathias Behrle ]
  * Merging branch debian-wheezy-2.2 (Closes: #687743).

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 17 Sep 2012 16:39:42 +0200

tryton-modules-account (2.4.2-1) experimental; urgency=low

  * Merging upstream version 2.4.2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 11 Sep 2012 13:26:34 +0200

tryton-modules-account (2.4.1-1) experimental; urgency=low

  * Updating to Standards-Version: 3.9.3, no changes needed.
  * Updating year in copyright.
  * Adding Format header for DEP5.
  * Merging upstream version 2.4.0.
  * Merging upstream version 2.4.1.
  * Updating Copyright.
  * Bumping versioned tryton depends to 2.4.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 26 Apr 2012 19:34:46 +0200

tryton-modules-account (2.2.1-1) unstable; urgency=low

  * Merging upstream version 2.2.1.
  * Bumping X-Python-Version to >=2.6.
  * Updating versioned tryton depends to 2.2.
  * Updating copyright.
  * Removing 01-dfsg-icons patch, patch went upstream.
  * Merging upstream version 2.2.0.
  * Removing tango-icon-theme from Depends.
  * Adding Bug URL to dfsg-icons patch.
  * Rediffing dfsg-icons patch.
  * Reordering anf fixing license for public-domain.

 -- Mathias Behrle <mathiasb@m9s.biz>  Mon, 26 Dec 2011 13:54:13 +0100

tryton-modules-account (2.0.1+dfsg-3) unstable; urgency=low

  * Removing deprecated XB-Python-Version for dh_python2.
  * Adding license for icons taken from tango project.
  * Adding myself to debian copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Fri, 22 Jul 2011 13:49:23 +0200

tryton-modules-account (2.0.1+dfsg-2) unstable; urgency=low

  * Patching in DFSG compatible icons instead of linking to them,
    setup.py needs to find them.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sat, 16 Jul 2011 23:29:59 +0200

tryton-modules-account (2.0.1+dfsg-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Removing for new source package version obsoleted README.source file.
  * Adding options for source package.
  * Compacting copyright file.
  * Not wrapping uploaders field, it does not exceed 80 chars.

  [ Mathias Behrle ]
  * Merging upstream version 2.0.1+dfsg.
  * Replacing non-DFSG compliant graphics by linking to tango-icon-theme.
  * Moving from deprecated python-support to dh_python2.
  * Adding pydist-overrides for dh_python2.

 -- Mathias Behrle <mathiasb@m9s.biz>  Thu, 14 Jul 2011 15:31:35 +0200

tryton-modules-account (2.0.1-1) unstable; urgency=low

  * Merging upstream version 2.0.1.

 -- Mathias Behrle <mathiasb@m9s.biz>  Sun, 05 Jun 2011 13:18:12 +0200

tryton-modules-account (2.0.0-1) unstable; urgency=low

  * Updating to standards version 3.9.2.
  * Merging upstream version 2.0.0.
  * Updating versioned tryton depends to 2.0.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 24 May 2011 21:28:28 +0200

tryton-modules-account (1.8.1-1) unstable; urgency=low

  * Changing my email address.
  * Setting minimal Python version to 2.5.
  * Merging upstream version 1.8.1.
  * Updating Copyright.

 -- Mathias Behrle <mathiasb@m9s.biz>  Tue, 15 Feb 2011 13:00:47 +0100

tryton-modules-account (1.8.0-1) experimental; urgency=low

  * Wrapping depends.
  * Updating standards version to 3.9.0.
  * Updating to debhelper version 8.
  * Updating to standards version 3.9.1.
  * Switching to source format 3.0 (quilt).
  * Merging upstream version 1.8.0.
  * Updating versioned tryton depends to 1.8.

 -- Daniel Baumann <daniel@debian.org>  Fri, 12 Nov 2010 12:50:05 +0100

tryton-modules-account (1.6.0-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Adding Dm-Upload-Allowed in control in preparation for Mathias.

  [ Mathias Behrle ]
  * Merging upstream version 1.6.0.
  * Updating copyright.
  * Updating depends.

 -- Mathias Behrle <mathiasb@mbsolutions.selfip.biz>  Wed, 12 May 2010 12:49:13 +0200

tryton-modules-account (1.4.2-1) unstable; urgency=low

  * Updating year in copyright file.
  * Removing unneeded python-all-dev from build-depends.
  * Updating to standards 3.8.4.
  * Merging upstream version 1.4.2.

 -- Daniel Baumann <daniel@debian.org>  Sat, 20 Feb 2010 09:44:05 +0100

tryton-modules-account (1.4.1-1) unstable; urgency=low

  * Making depends versioned for tryton 1.4.
  * Merging upstream version 1.4.1.
  * Updating README.source.
  * Adding explicit debian source version 1.0 until switch to 3.0.

 -- Daniel Baumann <daniel@debian.org>  Wed, 25 Nov 2009 12:47:48 +0100

tryton-modules-account (1.4.0-2) unstable; urgency=low

  * Reuploading package without changes, apparently the build process
    tainted tax.xml somehow (Closes: #552021).

 -- Daniel Baumann <daniel@debian.org>  Fri, 23 Oct 2009 07:22:20 +0200

tryton-modules-account (1.4.0-1) unstable; urgency=low

  * Merging upstream version 1.4.0.

 -- Daniel Baumann <daniel@debian.org>  Mon, 19 Oct 2009 21:22:20 +0200

tryton-modules-account (1.2.2-1) unstable; urgency=low

  * Updating to standards version 3.8.3.
  * Adding maintainer homepage field to control.
  * Adding README.source.
  * Merging upstream version 1.2.2.
  * Moving maintainer homepage field to copyright.
  * Updating README.source.
  * Using ascii only characters in copyright.

 -- Daniel Baumann <daniel@debian.org>  Sat, 05 Sep 2009 09:37:08 +0200

tryton-modules-account (1.2.1-3) unstable; urgency=low

  * Updating maintainer field.
  * Updating vcs fields.
  * Wrapping lines in control.

 -- Daniel Baumann <daniel@debian.org>  Mon, 10 Aug 2009 19:35:13 +0200

tryton-modules-account (1.2.1-2) unstable; urgency=low

  * Minimizing rules file.

 -- Daniel Baumann <daniel@debian.org>  Tue, 28 Jul 2009 10:56:25 +0200

tryton-modules-account (1.2.1-1) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Merging upstream version 1.2.1.

 -- Daniel Baumann <daniel@debian.org>  Fri, 10 Jul 2009 14:21:44 +0200

tryton-modules-account (1.2.0-1) unstable; urgency=low

  [ Daniel Baumann ]
  * Merging upstream version 1.2.0.
  * Tidy rules file.
  * Updating copyright file for new upstream release.
  * Including TODO file in docs.

  [ Mathias Behrle ]
  * Updating application description.

  [ Daniel Baumann ]
  * Correcting wrapping of control file.

 -- Daniel Baumann <daniel@debian.org>  Tue, 21 Apr 2009 20:16:00 +0200

tryton-modules-account (1.0.3-2) unstable; urgency=low

  * Adding Mathias to uploaders.

 -- Daniel Baumann <daniel@debian.org>  Sat, 28 Mar 2009 16:16:00 +0100

tryton-modules-account (1.0.3-1) unstable; urgency=low

  * Merging upstream version 1.0.3.

 -- Daniel Baumann <daniel@debian.org>  Mon, 23 Mar 2009 07:50:00 +0100

tryton-modules-account (1.0.2-1) unstable; urgency=low

  * Merging upstream version 1.0.2.

 -- Daniel Baumann <daniel@debian.org>  Mon, 23 Mar 2009 06:43:00 +0100

tryton-modules-account (1.0.1-1) unstable; urgency=low

  * Merging upstream version 1.0.1.
  * Updating standards to 3.8.1.
  * Making package arch all as it should be (Closes: #520795).

 -- Daniel Baumann <daniel@debian.org>  Sun, 22 Mar 2009 22:44:00 +0100

tryton-modules-account (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #506095).

 -- Daniel Baumann <daniel@debian.org>  Mon, 12 Jan 2009 15:49:00 -0500
